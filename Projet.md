# Projet

Le but de ce projet est de créer des fonctionnalités pour pouvoir :
* localiser les clefs du département au sein de l'iut,
* savoir avec qui est la clef,
* avoir une alerte pour savoir quand recharger la batterie de la clef

Pour cela, nous avons former un groupe. Ce groupe est composé de Huet David et moi-même, Botomisy Ludovic (LudoBoto). Notre nom de groupe est HuetOmisy ! 

>Se réunir est un début, rester ensemble est un progrès, travailler ensemble est la réussite.

## Première séance

Dans cette séance, la question posé est : **Comment localiser la clef ?**

Pour ce faire nous avons décidé d'utiliser les coordonnées GPS. Nous avons trouver plusieurs code. Nous avons commencé par le code exemple donné par l'IDE d'Arduino : 
````c
#include <LGPS.h>

gpsSentenceInfoStruct info;
char buff[256];

static unsigned char getComma(unsigned char num,const char *str)
{
  unsigned char i,j = 0;
  int len=strlen(str);
  for(i = 0;i < len;i ++)
  {
     if(str[i] == ',')
      j++;
     if(j == num)
      return i + 1; 
  }
  return 0; 
}

static double getDoubleNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atof(buf);
  return rev; 
}

static double getIntNumber(const char *s)
{
  char buf[10];
  unsigned char i;
  double rev;
  
  i=getComma(1, s);
  i = i - 1;
  strncpy(buf, s, i);
  buf[i] = 0;
  rev=atoi(buf);
  return rev; 
}

void parseGPGGA(const char* GPGGAstr)
{
  /* Refer to http://www.gpsinformation.org/dale/nmea.htm#GGA
   * Sample data: $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
   * Where:
   *  GGA          Global Positioning System Fix Data
   *  123519       Fix taken at 12:35:19 UTC
   *  4807.038,N   Latitude 48 deg 07.038' N
   *  01131.000,E  Longitude 11 deg 31.000' E
   *  1            Fix quality: 0 = invalid
   *                            1 = GPS fix (SPS)
   *                            2 = DGPS fix
   *                            3 = PPS fix
   *                            4 = Real Time Kinematic
   *                            5 = Float RTK
   *                            6 = estimated (dead reckoning) (2.3 feature)
   *                            7 = Manual input mode
   *                            8 = Simulation mode
   *  08           Number of satellites being tracked
   *  0.9          Horizontal dilution of position
   *  545.4,M      Altitude, Meters, above mean sea level
   *  46.9,M       Height of geoid (mean sea level) above WGS84
   *                   ellipsoid
   *  (empty field) time in seconds since last DGPS update
   *  (empty field) DGPS station ID number
   *  *47          the checksum data, always begins with *
   */
  double latitude;
  double longitude;
  int tmp, hour, minute, second, num ;
  if(GPGGAstr[0] == '$')
  {
    tmp = getComma(1, GPGGAstr);
    hour     = (GPGGAstr[tmp + 0] - '0') * 10 + (GPGGAstr[tmp + 1] - '0');
    minute   = (GPGGAstr[tmp + 2] - '0') * 10 + (GPGGAstr[tmp + 3] - '0');
    second    = (GPGGAstr[tmp + 4] - '0') * 10 + (GPGGAstr[tmp + 5] - '0');
    
    sprintf(buff, "UTC timer %2d-%2d-%2d", hour, minute, second);
    Serial.println(buff);
    
    tmp = getComma(2, GPGGAstr);
    latitude = getDoubleNumber(&GPGGAstr[tmp]);
    tmp = getComma(4, GPGGAstr);
    longitude = getDoubleNumber(&GPGGAstr[tmp]);
    sprintf(buff, "latitude = %10.4f, longitude = %10.4f", latitude, longitude);
    Serial.println(buff); 
    
    tmp = getComma(7, GPGGAstr);
    num = getIntNumber(&GPGGAstr[tmp]);    
    sprintf(buff, "satellites number = %d", num);
    Serial.println(buff); 
  }
  else
  {
    Serial.println("Not get data"); 
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  LGPS.powerOn();
  Serial.println("LGPS Power on, and waiting ..."); 
  delay(3000);
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println("LGPS loop"); 
  LGPS.getData(&info);
  Serial.println((char*)info.GPGGA); 
  parseGPGGA((const char*)info.GPGGA);
  delay(2000);
}
```

Le problème avec code, c'est qu'il ne retournait pas la bonne position. Il nous disait que la clef était bien sur l'île mais pas à l'IUT (Sur la route des tamarins plus précisément).

Alors, nous avons accentuer nos recherches sur la précision de la localisation. Nous avons trouver ce code :

````c
#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <LGPS.h>

//uncomment the storage you want to use
#define Drv LFlash          // use Internal 10M Flash
// #define Drv LSD           // use SD card

gpsSentenceInfoStruct info; //needed to get GPS data

double latitude = 0.00;
double longitude = 0.00;
float altitude = 0.00;
float dop = 100.00; //dilution of precision
float geoid = 0.00;
float k_speed = 0.00, m_speed = 0.00; //speed in knots and speed in m/s
float track_angle = 0.00;
int fix = 0;
int hour = 0, minute = 0, second = 0;
int sat_num = 0; //number of visible satellites
int day = 0, month = 0, year = 0;
String time_format = "00:00:00", date_format = "00:00:0000";
String lat_format = "0.00000", lon_format = "0.00000";
char file[15] = "";
int pause = 3000; //time in milliseconds between two logs

void setup()
{
  Serial.begin(115200);
  Serial.print("Initializing memory...");
  pinMode(10, OUTPUT); //needed for SD card
  if(!Drv.begin())
  {
    Serial.println("Error initalizing memory.");  
    while(true);
  }
  Serial.println("OK.");
  LGPS.powerOn();
  Serial.println("GPS started.");

}

void loop()
{
  if (getData(&info) > 3)
  {
    String str = "";
    str += date_format;
    str += ",";
    str += time_format;
    str += ",";
    str += lat_format;
    str += ",";
    str += lon_format;
    str += ",";
    str += altitude;
    str += ",";
    str += dop;
    str += ",";
    str += geoid;
    str += ",";
    str += track_angle;
    str += ",";
    str += m_speed;
    str += ",";
    str += k_speed;
    str += ",";
    str += fix;
    str += ",";
    str += sat_num;
    Serial.println(str);
    String file_name = date_format;
    file_name += ".txt";
    file_name.toCharArray(file, 14);
    Serial.println(file);
    LFile dataFile = Drv.open(file, FILE_WRITE);
    if (dataFile)
    {
      dataFile.println(str);
      dataFile.close();
      Serial.println("File written.");
    }
    else Serial.println("Error opening file.");


  }
  else Serial.println("Less then 4 satelites.");
  delay(pause);
}

/**
*Converts degrees from (d)ddmm.mmmm to (d)dd.mmmmmm
*@param str the string rappresentation of the angle in (d)ddmm.mmmm format
*@param dir if true the direction is south, and the angle is negative.
*@return the given angle in dd.mmmmmm format.
*/
float convert(String str, boolean dir)
{
  double mm, dd;
  int point = str.indexOf('.');
  dd = str.substring(0, (point - 2)).toFloat();
  mm = str.substring(point - 2).toFloat() / 60.00;
  return (dir ? -1 : 1) * (dd + mm);
}

/**
*Gets gps informations
*@param info gpsSentenceInfoStruct is a struct containing NMEA sentence infomation
*@return the number of hooked satellites, or 0 if there was an error getting informations
*/
int getData(gpsSentenceInfoStruct* info)
{
  Serial.println("Collecting GPS data.");
  LGPS.getData(info);
  Serial.println((char*)info->GPGGA);
  if (info->GPGGA[0] == '$')
  {
    Serial.print("Parsing GGA data....");
    String str = (char*)(info->GPGGA);
    str = str.substring(str.indexOf(',') + 1);
    hour = str.substring(0, 2).toInt();
    minute = str.substring(2, 4).toInt();
    second = str.substring(4, 6).toInt();
    time_format = "";
    time_format += hour;
    time_format += ":";
    time_format += minute;
    time_format += ":";
    time_format += second;
    str = str.substring(str.indexOf(',') + 1);
    latitude = convert(str.substring(0, str.indexOf(',')), str.charAt(str.indexOf(',') + 1) == 'S');
    int val = latitude * 1000000;
    String s = String(val);
    lat_format = s.substring(0, (abs(latitude) < 100) ? 2 : 3);
    lat_format += '.';
    lat_format += s.substring((abs(latitude) < 100) ? 2 : 3);
    str = str.substring(str.indexOf(',') + 3);
    longitude = convert(str.substring(0, str.indexOf(',')), str.charAt(str.indexOf(',') + 1) == 'W');
    val = longitude * 1000000;
    s = String(val);
    lon_format = s.substring(0, (abs(longitude) < 100) ? 2 : 3);
    lon_format += '.';
    lon_format += s.substring((abs(longitude) < 100) ? 2 : 3);

    str = str.substring(str.indexOf(',') + 3);
    fix = str.charAt(0) - 48;
    str = str.substring(2);
    sat_num = str.substring(0, 2).toInt();
    str = str.substring(3);
    dop = str.substring(0, str.indexOf(',')).toFloat();
    str = str.substring(str.indexOf(',') + 1);
    altitude = str.substring(0, str.indexOf(',')).toFloat();
    str = str.substring(str.indexOf(',') + 3);
    geoid = str.substring(0, str.indexOf(',')).toFloat();
    Serial.println("done.");
    
    if (info->GPRMC[0] == '$')
    {
      Serial.print("Parsing RMC data....");
      str = (char*)(info->GPRMC);
      int comma = 0;
      for (int i = 0; i < 60; ++i)
      {
        if (info->GPRMC[i] == ',')
        {
          comma++;
          if (comma == 7)
          {
            comma = i + 1;
            break;
          }
        }
      }

      str = str.substring(comma);
      k_speed = str.substring(0, str.indexOf(',')).toFloat();
      m_speed = k_speed * 0.514;
      str = str.substring(str.indexOf(',') + 1);
      track_angle = str.substring(0, str.indexOf(',')).toFloat();
      str = str.substring(str.indexOf(',') + 1);
      day = str.substring(0, 2).toInt();
      month = str.substring(2, 4).toInt();
      year = str.substring(4, 6).toInt();
      date_format = "20";
      date_format += year;
      date_format += "-";
      date_format += month;
      date_format += "-";
      date_format += day;
      Serial.println("done.");
      return sat_num;
    }
  }
  else
  {
    Serial.println("No GGA data");
  }
  return 0;
}
```

Ce code nous retourne non seulement la position à 2 mètres prêts mais en plus il nous donne :
* la date,
* la lattitude (avec qui nous avons un petit problème),
* la longitude,
* l'altitude,
* l'incertitude de position (2m prêt),
* geoid,
* l'angle de traque,
* la vitesse en noeud,
* la vitesse en m/s,
* la force du signal,
* le nombre de sattelite

## Problème rencontré

Nous avons un petit problème avec la lattitude. En effet, celle-ci nous ai donné avec une petite erreur de virgule. Au lieu d'avoir -21.34, nous avons -2.134. Ce qui est embêtant. Avant de passer à la suite, il faudrait régler ce problème.

La source du problème était qu'il affichait en sortie les caractères. Il était configurer pour afficher 2 caractères avant la virgule. le signe - étant considéré comme un caractère, faisait qu'il y avait ce problème. Du coup nous avons remplacer cette ligne :

````c
lat_format = s.substring(0, (abs(latitude) < 100) ? 2 : 3);
```
Par cette ligne :

````c
lat_format = s.substring(0, (abs(latitude) < 100) ? 3 : 2);
```
La différence est qu'on indique à la carte de prendre au moins 3 caractères avant la virgule.

### Pour la suite

A la prochaine séance, nous allons configurer une alarme pour nous alerter de l'état de la batterie.

*Source* : https://www.hackster.io/Momy93/gps-datalogger-with-linkit-one-fe6541


## Deuxième séance 

Dance cette séance, nous avons décidé de s'attaquer à la transmission des données GPS en Wifi. Pour cela, nous avons créer un code qui utilisera la carte comme serveur web.

Le but de cette séance était de comprendre comment fonctionne le code et de savoir comment afficher les données sur la page web. 

```c
/*
  WiFi Web Server

 A simple web server that a repeated counter

 Change the macro WIFI_AP, WIFI_PASSWORD and WIFI_AUTH accordingly.

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 modified 20 Aug 2014
 by MediaTek Inc.
 */
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>

#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration

LWiFiServer server(80);

void setup()
{
  LWiFi.begin();
  Serial.begin(115200);

  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  printWifiStatus();

  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");
}

int loopCount = 0;

void loop()
{
  // put your main code here, to run repeatedly:
  delay(500);
  loopCount++;
  LWiFiClient client = server.available();
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        // we basically ignores client request, but wait for HTTP request end
        int c = client.read();
        Serial.print((char)c);

        if (c == '\n' && currentLineIsBlank)
        {
          Serial.println("send response");
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin
          client.print("loop");
          client.print(" is ");
          client.print(loopCount);
          client.println("<br />");
          client.println("</html>");
          client.println();
          break;
        }
        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(500);

    // close the connection:
    Serial.println("close connection");
    client.stop();
    Serial.println("client disconnected");
  }
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
```

Ce code nous retourne l'adresse ip de notre serveur qui est 192.168.1.229 et génère une page html qui affiche le nombre de fois qu'on passe la loop.

Dans cette séance j'ai également allégé le code de la localisation GPS pour pouvoir récupéré les données qui m'intéresse et l'afficher sur la page web.

Nous avons mixer le code du WIFI et celui du GPS pour nous affiché les données GPS de la carte :

````c
#include <LFlash.h>
#include <LSD.h>
#include <LStorage.h>
#include <LGPS.h>
#define Drv LFlash 
#include <LWiFi.h>
#include <LWiFiServer.h>
#include <LWiFiClient.h>
#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP according to your WiFi AP configuration


gpsSentenceInfoStruct info; //needed to get GPS data
double latitude = 0.00;
double longitude = 0.00;
float altitude = 0.00;
float dop = 100.00; //dilution of precision
float geoid = 0.00;
float k_speed = 0.00, m_speed = 0.00; //speed in knots and speed in m/s
float track_angle = 0.00;
int fix = 0;
int hour = 0, minute = 0, second = 0;
int sat_num = 0; //number of visible satellites
int day = 0, month = 0, year = 0;
String time_format = "00:00:00", date_format = "00:00:0000";
String lat_format = "0.00000", lon_format = "0.00000";
char file[15] = "";
int pause = 3000; //time in milliseconds between two logs

LWiFiServer server(80);


void setup()
{
    Serial.begin(115200);
  Serial.print("Initializing memory...");
  pinMode(10, OUTPUT); //needed for SD card
  if(!Drv.begin())
  {
    Serial.println("Error initalizing memory.");  
    while(true);
  }
  Serial.println("OK.");
  LGPS.powerOn();
  Serial.println("GPS started.");

  
  LWiFi.begin();
  Serial.begin(115200);

  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  printWifiStatus();

  Serial.println("Start Server");
  server.begin();
  Serial.println("Server Started");
}

int loopCount = 0;

void loop()
{
  if (getData(&info) > 3)
  {
    String str = "";
    str += date_format;
    str += ",";
    str += time_format;
    str += ",";
    str += lat_format;
    str += ",";
    str += lon_format;
    str += ",";
    str += sat_num;
    Serial.println(str);
    String file_name = date_format;
    file_name += ".txt";
    file_name.toCharArray(file, 14);
    Serial.println(file);
    LFile dataFile = Drv.open(file, FILE_WRITE);
    if (dataFile)
    {
      dataFile.println(str);
      dataFile.close();
      Serial.println("File written.");
    }
    else Serial.println("Error opening file.");


  }
  else Serial.println("Less then 4 satelites.");
  delay(pause);


  // put your main code here, to run repeatedly:
  delay(500);
  loopCount++;
  LWiFiClient client = server.available();
  if (client)
  {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected())
    {
      if (client.available())
      {
        // we basically ignores client request, but wait for HTTP request end
        int c = client.read();
        Serial.print((char)c);

        if (c == '\n' && currentLineIsBlank)
        {
          Serial.println("send response");
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin
          client.print("heure : ");
          client.print(time_format);
          client.print("</br>");
          client.print("latitude : ");
          client.print(lat_format);
          client.print("</br>");
          client.print("longitude : ");
          client.print(lon_format);
          client.println("</html>");
          client.println();
          break;
        }
        if (c == '\n')
        {
          // you're starting a new line
          currentLineIsBlank = true;
        }
        else if (c != '\r')
        {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(500);

    // close the connection:
    Serial.println("close connection");
    client.stop();
    Serial.println("client disconnected");
  }
}

float convert(String str, boolean dir)
{
  double mm, dd;
  int point = str.indexOf('.');
  dd = str.substring(0, (point - 2)).toFloat();
  mm = str.substring(point - 2).toFloat() / 60.00;
  return (dir ? -1 : 1) * (dd + mm);
}

int getData(gpsSentenceInfoStruct* info)
{
  Serial.println("Collecting GPS data.");
  LGPS.getData(info);
  Serial.println((char*)info->GPGGA);
  if (info->GPGGA[0] == '$')
  {
    Serial.print("Parsing GGA data....");
    String str = (char*)(info->GPGGA);
    str = str.substring(str.indexOf(',') + 1);
    hour = str.substring(0, 2).toInt();
    minute = str.substring(2, 4).toInt();
    second = str.substring(4, 6).toInt();
    time_format = "";
    time_format += hour + 4;
    time_format += ":";
    time_format += minute;
    time_format += ":";
    time_format += second;
    str = str.substring(str.indexOf(',') + 1);
    latitude = convert(str.substring(0, str.indexOf(',')), str.charAt(str.indexOf(',') + 1) == 'S');
    int val = latitude * 1000000;
    String s = String(val);
    lat_format = s.substring(0, (abs(latitude) < 100) ? 3 : 2);
    lat_format += '.';
    lat_format += s.substring((abs(latitude) < 100) ? 2 : 3);
    str = str.substring(str.indexOf(',') + 3);
    longitude = convert(str.substring(0, str.indexOf(',')), str.charAt(str.indexOf(',') + 1) == 'W');
    val = longitude * 1000000;
    s = String(val);
    lon_format = s.substring(0, (abs(longitude) < 100) ? 2 : 3);
    lon_format += '.';
    lon_format += s.substring((abs(longitude) < 100) ? 2 : 3);

    str = str.substring(str.indexOf(',') + 3);
    fix = str.charAt(0) - 48;
    str = str.substring(2);
    sat_num = str.substring(0, 2).toInt();
    str = str.substring(3);
    dop = str.substring(0, str.indexOf(',')).toFloat();
    str = str.substring(str.indexOf(',') + 1);
    Serial.println("done.");    
    
    if (info->GPRMC[0] == '$')
    {
      Serial.print("Parsing RMC data....");
      str = (char*)(info->GPRMC);
      int comma = 0;
      for (int i = 0; i < 60; ++i)
      {
        if (info->GPRMC[i] == ',')
        {
          comma++;
          if (comma == 7)
          {
            comma = i + 1;
            break;
          }
        }
      }

      str = str.substring(comma);
      str = str.substring(str.indexOf(',') + 1);
      day = str.substring(0, 2).toInt();
      month = str.substring(2, 4).toInt();
      year = str.substring(4, 6).toInt();
      date_format = "20";
      date_format += year;
      date_format += "-";
      date_format += month;
      date_format += "-";
      date_format += day;
      Serial.println("done.");
      return sat_num;
    }
  }
  else
  {
    Serial.println("No GGA data");
  }
  return 0;
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());

  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
```
## Troisième séance

Aujourd'hui, nous avons travailler sur le module GSM pour que la carte puisse envoyer des informations à l'administrateur réseau pour qu'il sache avec qui la clef se trouve. Voici le code qui effectue ce travaille :

````c
#include "LGSM.h"


const char* contact = "0692374260";   //le sms sera envoyé sur ce numéro


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Initialisation du module GSM");
  while (!LSMS.ready()){             //Initialise le module GSM
    delay(1000);
    Serial.println(".");
  }

  Serial.println("Le SMS est prêt à être envoyé !");
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(120000);
  envoyer_sms();
}


//L'envoie du SMS
void envoyer_sms() {
  LSMS.beginSMS(contact);

  LSMS.print("Je suis avec Ludovic."); // Le nom est à préciser

  if (LSMS.endSMS()) {
    Serial.println("SMS envoyé !");
  }
  else {
    Serial.println("SMS non envoyé !");
  }
}
```

## Quatrième Séance

Pour cette séance nous nous sommes intéréssé à la position de la carte, c'est à dire que nous avons modifier le codes pour qu'il compare les latitudes et les longitudes pour nous donner un nom de salle.
